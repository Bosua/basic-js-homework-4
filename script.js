// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

// function createNewUser() {
//   let firstName = prompt("input your firstname");
//   let lastName = prompt("input your lastname");
//   let newUser = {
//     firstName: firstName,
//     lastName: lastName,
//     getLogin() {
//       return firstName[0].toLowerCase() + lastName.toLowerCase();
//     },
//   };
//   return newUser;
// }
// let user = createNewUser();
// console.log(user.getLogin());
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
function createNewUser() {
  let firstName = prompt("input your firstname");
  let lastName = prompt("input your lastname");
  let privateFirstName = firstName;
  let privateLastName = lastName;
  let newUser = {
    get firstName() {
      return privateFirstName;
    },
    get lastName() {
      return privateLastName;
    },
    setFirstName(newFirstName) {
      privateFirstName = newFirstName;
    },
    setLastName(newLastName) {
      privateLastName = newLastName;
    },
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  };

  return newUser;
}
let user = createNewUser();
console.log(user.getLogin());

user.setFirstName(prompt("input your new firstname "));
user.setLastName(prompt("input your new lastname"));

console.log(user.getLogin());
